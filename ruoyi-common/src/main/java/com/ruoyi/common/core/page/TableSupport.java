package com.ruoyi.common.core.page;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 表格数据处理
 *
 * @author ruoyi
 */
public class TableSupport {
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 分页参数合理化
     */
    public static final String REASONABLE = "reasonable";

    /**
     * 封装分页对象
     */
    public static PageDomain getPageDomain() {
        PageDomain pageDomain = new PageDomain();
        final HttpServletRequest request = ServletUtils.getRequest();
        JSONObject jsonObject = null;
        try {
            jsonObject = JSON.parseObject(request.getReader());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (jsonObject != null) {
            pageDomain.setPageNum(Convert.toInt(jsonObject.getString(PAGE_NUM), 1));
            pageDomain.setPageSize(Convert.toInt(jsonObject.getString(PAGE_SIZE), 10));
        } else {
            pageDomain.setPageNum(Convert.toInt(ServletUtils.getParameter(PAGE_NUM), 1));
            pageDomain.setPageSize(Convert.toInt(ServletUtils.getParameter(PAGE_SIZE), 10));
        }
        pageDomain.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        pageDomain.setIsAsc(ServletUtils.getParameter(IS_ASC));
        pageDomain.setReasonable(ServletUtils.getParameterToBool(REASONABLE));
        return pageDomain;
    }


    public static PageDomain buildPageRequest() {
        return getPageDomain();
    }
}
