package com.ruoyi.demo.reimbursement.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.silly.mybatisplus.MySillyMaster;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 报销申请单
 */
@TableName("silly_reimbursement")
@EqualsAndHashCode(callSuper = true)
@Data
public class ReimbursementMaster extends MySillyMaster<ReimbursementMaster> implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 流程类型，一个主类型唯一，不可修改
	 */
	public static final String CATEGORY = "silly_reimbursement";

	@Override
	public String usedCategory() {
		return CATEGORY;
	}

	/**
	 * 报销单编号
	 */
	private String code;
	/**
	 * 报销金额
	 */
	private BigDecimal money;
	/**
	 * 文件组ID
	 */
	private String fileGroupId;
	/**
	 * 备注
	 */
	private String remarks;

}
