package com.ruoyi.demo.reimbursement.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.demo.reimbursement.domain.ReimbursementVariable;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报销申请单 变量对象Dao
 */
@Mapper
public interface ReimbursementVariableMapper extends BaseMapper<ReimbursementVariable> {

}
