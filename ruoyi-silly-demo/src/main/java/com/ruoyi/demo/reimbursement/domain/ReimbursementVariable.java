package com.ruoyi.demo.reimbursement.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.silly.mybatisplus.MySillyVariable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 报销单变量表数据
 */
@TableName("silly_reimbursement_variable")
@EqualsAndHashCode(callSuper = true)
@Data
public class ReimbursementVariable extends MySillyVariable<ReimbursementVariable> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public String usedCategory() {
		return ReimbursementMaster.CATEGORY;
	}
}
