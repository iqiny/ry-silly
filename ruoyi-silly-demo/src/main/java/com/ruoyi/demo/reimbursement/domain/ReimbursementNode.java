package com.ruoyi.demo.reimbursement.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.iqiny.silly.core.base.core.SillyVariable;
import com.ruoyi.silly.mybatisplus.MySillyNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 报销申请单 节点对象，对应每一步操作
 */
@TableName("silly_reimbursement_node")
@EqualsAndHashCode(callSuper = true)
@Data
public class ReimbursementNode extends MySillyNode<ReimbursementNode> implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableField(exist = false)
	private List<ReimbursementVariable> variableList;

	@Override
	public List<ReimbursementVariable> getVariableList() {
		return variableList;
	}

	@Override
	public <V extends SillyVariable> void setVariableList(List<V> variableList) {
		this.variableList = (List<ReimbursementVariable>) variableList;
	}

	@Override
	public String usedCategory() {
		return ReimbursementMaster.CATEGORY;
	}
}
