package com.ruoyi.demo.reimbursement.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.demo.reimbursement.domain.ReimbursementNode;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报销申请单 节点Dao
 */
@Mapper
public interface ReimbursementNodeMapper extends BaseMapper<ReimbursementNode> {

}
