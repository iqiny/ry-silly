package com.ruoyi.demo.reimbursement.service;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.stereotype.Service;

@Service
public class ReimbursementService {

    /**
     * 生成报销单编号 （json配置中调用）
     */
    public String generatorCode(String masterId, String projectCode) {
        return "BXD_" + projectCode + "_" + DateUtils.dateTimeNow() + "_" + masterId;
    }

}
