package com.ruoyi.demo.reimbursement.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.demo.reimbursement.domain.ReimbursementMaster;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报销申请单 主数据对象Dao
 */
@Mapper
public interface ReimbursementMapper extends BaseMapper<ReimbursementMaster> {

}
