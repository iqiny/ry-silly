package com.ruoyi.silly.mybatisplus;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.iqiny.silly.core.base.SillyCategory;
import com.iqiny.silly.core.base.core.SillyNode;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * SillyNode 集成MybatisPlus
 *
 * @param <T>
 */
public abstract class MySillyNode<T extends Model<T>> extends MyBaseEntity<T> implements SillyNode, SillyCategory {

    protected String nodeName;

    @TableField(exist = false)
    protected String nodeUserName;

    protected String masterId;

    protected Integer seq;

    protected String nodeKey;

    protected String taskId;

    protected String multipleFlag;

    protected Date nodeDate;
    protected String nodeUserId;

    protected String status;

    protected String nodeInfo;

    @TableField(exist = false)
    protected String handleType;

    @TableField(exist = false)
    protected Map<String, Object> variableMap = new LinkedHashMap<>();

    @Override
    public String getNodeName() {
        return nodeName;
    }

    @Override
    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    @Override
    public String getMasterId() {
        return masterId;
    }

    @Override
    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    @Override
    public Integer getSeq() {
        return seq;
    }

    @Override
    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    @Override
    public String getNodeKey() {
        return nodeKey;
    }

    @Override
    public void setNodeKey(String nodeKey) {
        this.nodeKey = nodeKey;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String getMultipleFlag() {
        return multipleFlag;
    }

    @Override
    public void setMultipleFlag(String multipleFlag) {
        this.multipleFlag = multipleFlag;
    }

    @Override
    public String getNodeUserId() {
        return nodeUserId;
    }

    @Override
    public void setNodeUserId(String nodeUserId) {
        this.nodeUserId = nodeUserId;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getNodeInfo() {
        return nodeInfo;
    }

    @Override
    public void setNodeInfo(String nodeInfo) {
        this.nodeInfo = nodeInfo;
    }

    @Override
    public String getHandleType() {
        return handleType;
    }

    @Override
    public void setHandleType(String handleType) {
        this.handleType = handleType;
    }

    @Override
    public Map<String, Object> getVariableMap() {
        return variableMap;
    }

    @Override
    public void setVariableMap(Map<String, Object> variableMap) {
        this.variableMap = variableMap;
    }

    public String getNodeUserName() {
        if (nodeUserName == null) {
            //nodeUserName = UserUtils.getUserName(nodeUserId);
        }
        return nodeUserName;
    }

    public void setNodeUserName(String nodeUserName) {
        this.nodeUserName = nodeUserName;
    }

    @Override
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getNodeDate() {
        return nodeDate;
    }

    @Override
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public void setNodeDate(Date nodeDate) {
        this.nodeDate = nodeDate;
    }

}
