package com.ruoyi.silly.mybatisplus;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.iqiny.silly.core.base.SillyCategory;
import com.iqiny.silly.core.base.core.SillyVariable;

/**
 * SillyVariable 集成MybatisPlus
 *
 * @param <T>
 */
public abstract class MySillyVariable<T extends Model<T>> extends MyBaseEntity<T> implements SillyVariable, SillyCategory {

    protected String nodeId;
    protected String masterId;

    protected String taskId;
    protected String nodeKey;
    protected String status;
    protected Integer seq;

    protected String engineVariableType;

    protected String variableType;
    protected String variableName;
    protected String variableText;
    protected String belong;

    protected String groupId;
    protected String originalVariableName;

    @Override
    public String getNodeId() {
        return nodeId;
    }

    @Override
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @Override
    public String getMasterId() {
        return masterId;
    }

    @Override
    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String getNodeKey() {
        return nodeKey;
    }

    @Override
    public void setNodeKey(String nodeKey) {
        this.nodeKey = nodeKey;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Integer getSeq() {
        return seq;
    }

    @Override
    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    @Override
    public String getEngineVariableType() {
        return engineVariableType;
    }

    @Override
    public void setEngineVariableType(String engineVariableType) {
        this.engineVariableType = engineVariableType;
    }

    @Override
    public String getVariableType() {
        return variableType;
    }

    @Override
    public void setVariableType(String variableType) {
        this.variableType = variableType;
    }

    @Override
    public String getVariableName() {
        return variableName;
    }

    @Override
    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    @Override
    public String getVariableText() {
        return variableText;
    }

    @Override
    public void setVariableText(String variableText) {
        this.variableText = variableText;
    }

    @Override
    public String getBelong() {
        return belong;
    }

    @Override
    public void setBelong(String belong) {
        this.belong = belong;
    }

    @Override
    public String getGroupId() {
        return groupId;
    }

    @Override
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public String getOriginalVariableName() {
        return originalVariableName;
    }

    @Override
    public void setOriginalVariableName(String originalVariableName) {
        this.originalVariableName = originalVariableName;
    }
}
