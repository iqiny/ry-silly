package com.ruoyi.silly.mybatisplus;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.iqiny.silly.common.util.StringUtils;
import com.iqiny.silly.core.base.core.SillyEntity;
import com.iqiny.silly.core.event.crud.SillyEntityOrderBy;
import com.ruoyi.common.utils.SecurityUtils;

import java.util.Date;

/**
 * MybatisPlus 集成基础实体数据
 *
 * @param <T>
 */
public abstract class MyBaseEntity<T extends Model<T>> extends Model<T> implements SillyEntity {

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    protected String id;
    /**
     * 删除标记  1：已删除  0：正常
     */
    @TableLogic
    @JsonIgnore
    protected String delFlag;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @SillyEntityOrderBy(sort = 0)
    protected Date createDate;
    /**
     * 创建人
     */
    protected String createUserId;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @SillyEntityOrderBy(sort = 1)
    protected Date updateDate;
    /**
     * 更新人
     */
    protected String updateUserId;

    /**
     * 更新人
     */
    @TableField(exist = false)
    protected String updateUserName;
    /**
     * 创建人
     */
    @TableField(exist = false)
    protected String createUserName;

    public void preInsert() {
        if (isNewRecord()) {
            id = null;
        }
        this.createDate = new Date();
        this.createUserId = SecurityUtils.getUserId().toString();
        this.updateDate = new Date();
        this.updateUserId = SecurityUtils.getUserId().toString();
        this.delFlag = "0";
    }

    public void preUpdate() {
        this.updateDate = new Date();
        this.updateUserId = SecurityUtils.getUserId().toString();
        this.delFlag = "0";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 判断是否为新数据
     */
    @JsonIgnore
    public boolean isNewRecord() {
        return StringUtils.isEmpty(id);
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
}
