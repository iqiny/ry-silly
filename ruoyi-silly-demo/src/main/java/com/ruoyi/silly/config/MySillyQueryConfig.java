/*
 *  Copyright  iqiny.com
 *
 *  https://gitee.com/iqiny/silly
 *
 *  project name：silly-mybatisplus-2
 *  project description：top silly project pom.xml file
 */
package com.ruoyi.silly.config;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iqiny.silly.core.base.SillyOrdered;
import com.iqiny.silly.core.readhandle.page.BaseQueryPageConfig;
import com.ruoyi.common.core.page.TableSupport;
import org.springframework.stereotype.Component;

/**
 * 查询参数
 */
@Component
public class MySillyQueryConfig extends BaseQueryPageConfig<IPage<?>> implements SillyOrdered {

    @Override
    public String pageNoField() {
        return TableSupport.PAGE_NUM;
    }

    @Override
    public String limitField() {
        return TableSupport.PAGE_SIZE;
    }

    @Override
    public String totalField() {
        return "total";
    }

    @Override
    public String recordsField() {
        return "rows";
    }

    @Override
    protected int defaultLimit() {
        return 10000;
    }

    @Override
    protected IPage<?> createPage(int curPage, int limit) {
        return new Page<>(curPage, limit);
    }

    @Override
    public int order() {
        return -100;
    }
}
