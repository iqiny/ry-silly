package com.ruoyi.silly.config;

import com.iqiny.silly.spring.controller.SillyResultWrapper;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.stereotype.Component;

@Component
public class MySillyResultWrapper extends SillyResultWrapper {

    @Override
    public Object wrapperOk(Object obj) {
        return AjaxResult.success(obj);
    }

    @Override
    public Object wrapperError(String msg) {
        return AjaxResult.error(msg);
    }

}
