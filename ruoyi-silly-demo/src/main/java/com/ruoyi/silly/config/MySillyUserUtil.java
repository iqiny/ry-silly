package com.ruoyi.silly.config;

import com.iqiny.silly.common.util.SillyThreadDataUtil;
import com.iqiny.silly.common.util.StringUtils;
import com.iqiny.silly.core.cache.SillyCache;
import com.iqiny.silly.core.config.BaseSillyUserUtil;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class MySillyUserUtil extends BaseSillyUserUtil {

    private static final String CACHE_KEY = MySillyUserUtil.class.getName();

    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private SillyCache sillyCache;

    @Override
    public boolean isAdmin() {
        return isAdmin(currentUserId());
    }

    @Override
    public boolean isAdmin(String userId) {
        return getUserById(userId).isAdmin();
    }

    @Override
    public String currentUserId() {
        final String currentUserId = SillyThreadDataUtil.getThreadData(this);
        return StringUtils.isEmpty(currentUserId) ? SecurityUtils.getUserId().toString() : currentUserId;
    }

    @Override
    public String userIdToName(String userId) {
        return getUserById(userId).getNickName();
    }

    @Override
    public String userIdToOrgId(String userId) {
        return getUserById(userId).getDeptId().toString();
    }

    @Override
    public String userIdToOrgName(String userId) {
        return getUserById(userId).getDept().getDeptName();
    }

    @Override
    public void setCurrentUser(String userId) {
        SillyThreadDataUtil.setThreadData(this, userId);
    }

    @Override
    protected void recordAllUser() {
        final List<SysUser> userList = sysUserService.selectAllUserList();
        for (SysUser user : userList) {
            sillyCache.setValue(CACHE_KEY, user.getUserId(), user);
            final SysDept office = user.getDept();
            StringBuilder sb = new StringBuilder();
            if (office != null && StringUtils.isNotEmpty(office.getDeptName())) {
                sb.append(office.getDeptName()).append("-");
            }
            sb.append(user.getNickName()).append("(").append(user.getUserName()).append(")");
            recordUser(user.getUserId().toString(), sb.toString());
        }
    }

    protected SysUser getUserById(String userId) {
        SysUser sysUser = sillyCache.getValue(CACHE_KEY, userId);
        if (sysUser == null) {
            sysUser = sysUserService.selectUserById(Long.valueOf(userId));
            sillyCache.setValue(CACHE_KEY, userId, sysUser);
        }
        return sysUser;
    }
}
