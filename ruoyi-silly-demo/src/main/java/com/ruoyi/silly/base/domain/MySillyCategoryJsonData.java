package com.ruoyi.silly.base.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.iqiny.silly.core.data.SillyCategoryJsonData;
import com.ruoyi.silly.mybatisplus.MyBaseEntity;


/**
 * 操作数据对象
 *
 * @author QINY
 * @since 2019-07-30
 */
@TableName(value = "SILLY_CATEGORY_JSON_DATA", autoResultMap = true)
public class MySillyCategoryJsonData extends MyBaseEntity<MySillyCategoryJsonData> implements SillyCategoryJsonData {

    @TableField("JSON_KEY")
    private String jsonKey;

    @TableField("CATEGORY")
    private String category;

    @TableField("STATUS")
    private String status;

    @TableField("JSON_DATA")
    private String jsonData;

    @Override
    public String getJsonKey() {
        return jsonKey;
    }

    @Override
    public void setJsonKey(String jsonKey) {
        this.jsonKey = jsonKey;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getJsonData() {
        return jsonData;
    }

    @Override
    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }
}
