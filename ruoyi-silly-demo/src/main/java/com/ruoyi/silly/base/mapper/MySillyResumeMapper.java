package com.ruoyi.silly.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.silly.base.domain.MySillyResume;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MySillyResumeMapper extends BaseMapper<MySillyResume> {

}
