package com.ruoyi.silly.base.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.iqiny.silly.common.util.SillyObjectUtil;
import com.iqiny.silly.core.data.SillyOptionData;
import com.ruoyi.silly.mybatisplus.MyBaseEntity;

import java.util.Map;

/**
 * 操作数据对象
 *
 * @author QINY
 * @since 2019-07-30
 */
@TableName(value = "SILLY_SAVE_OPTION_DATA", autoResultMap = true)
public class MySillyOptionData extends MyBaseEntity<MySillyOptionData> implements SillyOptionData {

    @TableField("CATEGORY")
    private String category;

    @TableField("MASTER_ID")
    private String masterId;

    @TableField("NODE_KEY")
    private String nodeKey;

    @TableField("TASK_ID")
    private String taskId;

    @TableField("SEQ")
    private Integer seq;

    @TableField("STATUS")
    private String status;

    @TableField("OPTION_DATA")
    private String clobStr;

    @TableField(exist = false)
    private transient Map<String, Object> optionData;

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String getMasterId() {
        return masterId;
    }

    @Override
    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    @Override
    public String getNodeKey() {
        return nodeKey;
    }

    @Override
    public void setNodeKey(String nodeKey) {
        this.nodeKey = nodeKey;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public Integer getSeq() {
        return seq;
    }

    @Override
    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    public String getClobStr() {
        return clobStr;
    }

    public void setClobStr(String clobStr) {
        this.clobStr = clobStr;
    }

    @Override
    public Map<String, Object> getOptionData() {
        if (optionData == null && clobStr != null) {
            optionData = SillyObjectUtil.jsonToObject(clobStr, Map.class);
        }
        return optionData;
    }

    @Override
    public void setOptionData(Map<String, Object> optionData) {
        this.optionData = optionData;
        setClobStr(SillyObjectUtil.objectToJsonString(optionData));
    }
}
