package com.ruoyi.silly.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.silly.base.domain.MySillyCategoryJsonData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MySillyCategoryJsonDataMapper extends BaseMapper<MySillyCategoryJsonData> {

}
