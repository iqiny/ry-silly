/*
 *  Copyright  iqiny.com
 *
 *  https://gitee.com/iqiny/silly
 *
 *  project name：silly-mybatisplus-2.3.3
 *  project description：top silly project pom.xml file
 */
package com.ruoyi.silly.handle;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.Page;
import com.iqiny.silly.core.base.SillyOrdered;
import com.iqiny.silly.core.service.crud.SillyCrudService;
import com.iqiny.silly.mybatisplus.readhandle.SillyMyBatisPlusQueryPageReadHandle;
import com.ruoyi.common.utils.PageUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class MySillyQueryPageReadHandle extends SillyMyBatisPlusQueryPageReadHandle implements SillyOrdered {

    @Override
    public List<?> query(SillyCrudService crudService, Object page, Map<String, Object> params) {
        // 使用 本系统分页拦截器 执行分页 否则不会进行 count 查询
        PageUtils.startPage();
        final List<?> query = super.query(crudService, page, params);
        if (query instanceof Page) {
            final IPage<?> iPage = checkPage(page);
            iPage.setTotal(((Page) query).getTotal());
        }
        return query;
    }

    @Override
    public int order() {
        return -100;
    }
}
