package com.ruoyi.silly.handle;

import com.iqiny.silly.core.config.SillyCategoryConfig;
import com.iqiny.silly.core.savehandle.SillyOnceSourceData;
import com.iqiny.silly.core.savehandle.node.BaseSillyNodeSaveHandle;
import org.springframework.stereotype.Component;

/**
 * 模拟邮件发送处理器
 */
@Component
public class SendEmailNodeSaveHandle extends BaseSillyNodeSaveHandle {

    @Override
    public String name() {
        return "sendEmail";
    }

    @Override
    protected boolean canDo(SillyOnceSourceData sourceData) {
        return sourceData.isSubmit();
    }

    @Override
    protected void handle(SillyCategoryConfig sillyConfig, SillyOnceSourceData sourceData) {
        // 发送邮件逻辑
        System.out.println("假装发送邮件地址：" + sourceData.getVariableText("email"));
        System.out.println("假装发送邮件内容：" + sourceData.getVariableText("emailInfo"));
    }
}
