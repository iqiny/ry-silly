/*
 *  Copyright  iqiny.com
 *
 *  https://gitee.com/iqiny/silly
 *
 *  project name：silly-mybatisplus-3
 *  project description：top silly project pom.xml file
 */
package com.ruoyi.silly.handle;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iqiny.silly.core.base.SillyOrdered;
import com.iqiny.silly.core.readhandle.BaseSillyQueryPageResultReadHandle;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MySillyQueryPageResultReadHandle extends BaseSillyQueryPageResultReadHandle<IPage> implements SillyOrdered {

    @Override
    protected IPage checkPage(Object page) {
        if (page instanceof IPage) {
            return (IPage<?>) page;
        }
        return new Page();
    }

    @Override
    public Object resultToPage(List<?> resultList, Object page) {
        final IPage iPage = checkPage(page);
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(resultList);
        rspData.setTotal(iPage.getTotal());
        return rspData;
    }

    @Override
    public int order() {
        return -100;
    }
}