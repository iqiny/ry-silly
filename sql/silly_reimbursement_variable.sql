-- ----------------------------
-- Table structure for silly_reimbursement_variable
-- ----------------------------
DROP TABLE IF EXISTS silly_reimbursement_variable;
CREATE TABLE silly_reimbursement_variable
(
    ID                     bigint  NOT NULL AUTO_INCREMENT,
    CREATE_USER_ID         varchar(64) NULL DEFAULT NULL COMMENT '创建人ID',
    CREATE_DATE            datetime NULL DEFAULT NULL COMMENT '创建时间',
    UPDATE_USER_ID         varchar(64) NULL DEFAULT NULL COMMENT '更新人ID',
    UPDATE_DATE            datetime NULL DEFAULT NULL COMMENT '更新时间',
    DEL_FLAG               char(1) NOT NULL DEFAULT '0' COMMENT '删除标识',
    STATUS                 varchar(32) NULL DEFAULT NULL COMMENT '10: 当前数据 20：历史数据',
    NODE_ID                bigint NULL DEFAULT NULL COMMENT '节点数据ID',
    MASTER_ID              bigint NULL DEFAULT NULL COMMENT '主数据ID',
    TASK_ID                varchar(64) NULL DEFAULT NULL COMMENT '任务ID',
    NODE_KEY               varchar(64) NULL DEFAULT NULL COMMENT '节点Key',
    ENGINE_VARIABLE_TYPE   varchar(64) NULL DEFAULT NULL COMMENT '流程变量类型',
    VARIABLE_TYPE          varchar(255) NULL DEFAULT NULL COMMENT '变量类型',
    VARIABLE_NAME          varchar(255) NULL DEFAULT NULL COMMENT '变量名称',
    VARIABLE_TEXT          varchar(1024) NULL DEFAULT NULL COMMENT '变量内容',
    ORIGINAL_VARIABLE_NAME varchar(255) NULL DEFAULT NULL COMMENT '原始变量名',
    BELONG                 varchar(64) NULL DEFAULT NULL COMMENT '归属对象',
    SEQ                    bigint NULL DEFAULT NULL COMMENT '排序号',
    GROUP_ID               varchar(64) NULL DEFAULT NULL COMMENT '分组id',
    PRIMARY KEY (ID) USING BTREE,
    INDEX                  index_MASTER_ID(MASTER_ID ASC) USING BTREE,
    INDEX                  index_NODE_ID(NODE_ID ASC) USING BTREE,
    INDEX                  index_NODE_KEY(NODE_KEY ASC) USING BTREE,
    INDEX                  index_VARIABLE_NAME(VARIABLE_NAME ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 189;

