-- ----------------------------
-- 1、Silly工具每次操作的数据备份
-- ----------------------------
drop table if exists SILLY_SAVE_OPTION_DATA;
create table SILLY_SAVE_OPTION_DATA
(
    ID             bigint(20) not null auto_increment comment '主键',
    CREATE_USER_ID bigint(20) default null comment '创建人ID',
    CREATE_DATE    datetime     default null comment '创建时间',
    UPDATE_USER_ID bigint(20) default null comment '更新人ID',
    UPDATE_DATE    datetime     default null comment '更新时间',
    DEL_FLAG       char(1)      default '0' comment '删除标识符',
    CATEGORY       varchar(255) default null comment '业务分类',
    MASTER_ID      bigint(20) default null comment '主表ID',
    NODE_KEY       varchar(255) default null comment '节点ID（无任务ID时，每次操作会覆盖相同nodeKey数据）',
    TASK_ID        varchar(64)  default null comment '任务ID（有任务ID时，每次操作会覆盖相同taskId数据）',
    SEQ            int          default null comment '排序',
    OPTION_DATA    json         default null comment '操作的数据（表单提交的数据）',
    STATUS         varchar(10)  default null comment '数据状态 10：当前  20：历史',
    primary key (ID)
) engine=innodb auto_increment=200 comment = 'Silly工具每次操作的数据备份';

-- ----------------------------
-- 2、Silly流程履历表
-- ----------------------------
drop table if exists SILLY_RESUME;
create table SILLY_RESUME
(
    ID                bigint(20) not null auto_increment comment '主键',
    CREATE_USER_ID    bigint(20) default null comment '创建人ID',
    CREATE_DATE       datetime      default null comment '创建时间',
    UPDATE_USER_ID    bigint(20) default null comment '更新人ID',
    UPDATE_DATE       datetime      default null comment '更新时间',
    DEL_FLAG          char(1)       default '0' comment '删除标识符',
    CATEGORY          varchar(255)  default null comment '业务分类',
    MASTER_ID         bigint(20) default null comment '主表ID',
    NODE_KEY          varchar(255)  default null comment '节点ID',
    NODE_NAME         varchar(255)  default null comment '节点名称',
    SEQ               int           default null comment '排序',
    HANDLE_DEPT_ID    bigint(20) default null comment '操作部门ID',
    HANDLE_USER_ID    bigint(20) default null comment '操作人员ID',
    NEXT_USER_ID      varchar(1000) default null comment '下一步操作人ID， 多人逗号拼接',
    HANDLE_DATE       datetime      default null comment '处置时间',
    HANDLE_INFO       varchar(1000) default null comment '处置内容',
    PROCESS_TYPE      varchar(30)   default null comment '流程类型',
    PROCESS_LEVEL     int           default null comment '流程等级',
    CONSUME_TIME      bigint(20) default null comment '操作耗时',
    CONSUME_DEPT_TIME bigint(20) default null comment '部门操作耗时',
    primary key (ID)
) engine=innodb auto_increment=200 comment = 'Silly流程履历表';

-- ----------------------------
-- 3、Silly工具业务JSON表
-- ----------------------------
CREATE TABLE `SILLY_CATEGORY_JSON_DATA`
(
    `ID`             bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
    `CREATE_USER_ID` bigint       DEFAULT NULL COMMENT '创建人ID',
    `CREATE_DATE`    datetime     DEFAULT NULL COMMENT '创建时间',
    `UPDATE_USER_ID` bigint       DEFAULT NULL COMMENT '更新人ID',
    `UPDATE_DATE`    datetime     DEFAULT NULL COMMENT '更新时间',
    `DEL_FLAG`       char(1)      DEFAULT '0' COMMENT '删除标识符',
    `CATEGORY`       varchar(255) DEFAULT NULL COMMENT '业务分类',
    `KEY`            varchar(64)  DEFAULT NULL COMMENT 'JSONKEY 默认为JSON的MD5值',
    `JSON_DATA`      json         DEFAULT NULL COMMENT '业务JSON',
    `STATUS`         varchar(10)  DEFAULT NULL COMMENT '数据状态 10：当前  20：历史',
    PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=200 COMMENT='Silly工具业务JSON表';