-- ----------------------------
-- Table structure for silly_reimbursement_node
-- ----------------------------
DROP TABLE IF EXISTS silly_reimbursement_node;
CREATE TABLE silly_reimbursement_node
(
    id             bigint  NOT NULL AUTO_INCREMENT,
    MASTER_ID      bigint  NOT NULL COMMENT '主表ID',
    SEQ            int NULL DEFAULT NULL COMMENT '处置排序',
    NODE_KEY       varchar(64) NULL DEFAULT NULL COMMENT '节点Key',
    NODE_NAME      varchar(255) NULL DEFAULT NULL COMMENT '节点名称',
    TASK_ID        varchar(64) NULL DEFAULT NULL COMMENT '任务ID',
    MULTIPLE_FLAG  char(1) NOT NULL DEFAULT '0' COMMENT '并行标识 ‘1’ 是  ‘0’ 不是',
    NODE_DATE      datetime NULL DEFAULT NULL COMMENT '节点处置时间',
    NODE_USER_ID   varchar(64) NULL DEFAULT NULL COMMENT '节点处置人ID',
    STATUS         varchar(32) NULL DEFAULT NULL COMMENT '10 当前数据  20历史数据',
    NODE_INFO      varchar(512) NULL DEFAULT NULL COMMENT '节点信息',
    DEL_FLAG       char(1) NOT NULL DEFAULT '0',
    CREATE_DATE    datetime NULL DEFAULT NULL,
    CREATE_USER_ID varchar(64) NULL DEFAULT NULL,
    UPDATE_DATE    datetime NULL DEFAULT NULL,
    UPDATE_USER_ID varchar(64) NULL DEFAULT NULL,
    PRIMARY KEY (id) USING BTREE,
    INDEX          idex_MASTER_ID(MASTER_ID ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24;
