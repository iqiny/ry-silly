-- ----------------------------
-- Table structure for silly_reimbursement
-- ----------------------------
DROP TABLE IF EXISTS silly_reimbursement;
CREATE TABLE silly_reimbursement
(
    ID               bigint  NOT NULL AUTO_INCREMENT,
    CREATE_USER_ID   varchar(64) DEFAULT NULL COMMENT '创建人ID',
    CREATE_DATE      datetime DEFAULT NULL COMMENT '创建时间',
    UPDATE_USER_ID   varchar(64) DEFAULT NULL COMMENT '更新人ID',
    UPDATE_DATE      datetime DEFAULT NULL COMMENT '更新时间',
    DEL_FLAG         char(1) NOT NULL DEFAULT '0' COMMENT '删除标识',
    STATUS           varchar(32) DEFAULT NULL COMMENT '状态 （10: 编辑中 20：流程处置中   90：已完成 ）',
    PROCESS_ID       varchar(64) DEFAULT NULL COMMENT '流程实例ID',
    START_DATE       datetime DEFAULT NULL COMMENT '启动日期',
    START_USER_ID    varchar(64) DEFAULT NULL COMMENT '启动人',
    CLOSE_DATE       datetime DEFAULT NULL COMMENT '关闭日期',
    CLOSE_USER_ID    varchar(64) DEFAULT NULL COMMENT '关闭人',
    PROCESS_KEY      varchar(64) DEFAULT NULL COMMENT '流程KEY',
    PROCESS_VERSION  varchar(32) DEFAULT NULL COMMENT '流程版本',
    HANDLE_USER_NAME varchar(255) DEFAULT NULL COMMENT '当前处置人名称',
    TASK_NAME        varchar(255) DEFAULT NULL COMMENT '当前任务名称',
    code             varchar(255) DEFAULT NULL COMMENT '报销单编号',
    money            decimal(10, 0) DEFAULT NULL COMMENT '报销金额',
    file_group_id    varchar(64) DEFAULT NULL COMMENT '报销文件组ID',
    remarks          varchar(255) DEFAULT NULL COMMENT '备注说明',
    PRIMARY KEY (ID) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81;

